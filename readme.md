<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

# Marks

Very good guide of `TailWind + Vue + Laravel`. Loveit

## About the project

Following the course https://nick-basile.com/blog/post/setting-up-tailwind-in-a-laravel-project, let's implement a simple Vue + TailwindCSS + Laravel Project. 

`Html + CSS from TailWindCSS`

BackEnd Laravel


### Vue Project Characteristics and tricks
I like this 'simple' projects because it got lots of tricks, heritage, emits... just in a simple project!!

Please, take a look at the Components to understand it.

## Requirements

Vue and Tailwind Requirements
```bash
    npm install
    npm install laravel-mix --save-dev
    cp -r node_modules/laravel-mix/setup/** ./
    npm install tailwindcss --save-dev
    ./node_modules/.bin/tailwind init tailwind.js

    
```


Laravel and DB Requirements
DB must be defined at the *.env* file
```bash
    php artisan migrate
```

## Comments about development

We've created an specific file named `/resources/assets/js/sass/index.sass`

We've added the following lines into the `webpack.mix.js` file
```js
    let tailwindcss = require('tailwindcss');
    mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/index.sass', 'public/css/app.css')
    .options({
      processCssUrls: false,
      postCss: [ tailwindcss('./tailwind.js') ],
    });
```

We've created a `Model, Controller, Factory (Fake) and a Migration` for a todo table
```bash
    php artisan make:model Todo -a
```

In the Factory File created we've implemented:
```php
return [
        'text' => $faker->sentence,
        'finished' => false,
    ];
```

and for generating fake data, we've used `tinker`
```bash
    php artisan tinker

    factory(App\Todo::class, 5)->create()

```

where the number 5 is the number of items to create.
If in tinker we execute:

```bash
    Todo::all();
```
we'll get what we've generated

### Communication between components

It is very interesting how this project manage communications between the father-sons. In this case, father is a list of element, and the son are the elements. 

All Axios implementation for adding, removing, updating etc is located and managed at the father. The father includes a function calle initListeners triggered when the component is created (father). So, inside mehods:

```js
            initListeners() {
                const t = this;

                bus.$on('update-todo', function (details) {
                  t.updateTodo(details);
                })

               
            }
```
* `bus` is a global var. Must be declared at the *app.js* as  `window.bus = new Vue();`
* `updateTodo` is a method that use `Axios` (or any other library/functionality) for updating data
* `update-todo` is the function called from the son (emit)

```js
    updateTodo() {
        const t = this;

        t.$nextTick(() => {
            bus.$emit('update-todo', {data: t.data, index: t.index, id: t.todo.id});
        })
```

### Routing
It is very important to understand how routes are managed.

By adding to the `routes/web.php` the following:
```php
    Route::apiResource('todos', 'TodoController');
```
System will create special routes depending of the funcions created at the controller. (`php artisan route:list`)
i.e. for todos.show (get) a route todos/{todo} is automatically created

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    /**
     * fillable means that we allow to the fields text and finished to be created or updated. BUT NOT OTHERS
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'finished',
    ];

    /**
     * CAST is usefull for managing values. This mean that in the DB for the field finished 0 and 1 will be stored, but with this cast we will be able to use true/false
     *
     * @var array
     */
    protected $casts = [
        'finished' => 'boolean',
    ];
}
